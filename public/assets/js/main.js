(()=>{var e,r={579:(e,r,o)=>{"use strict";o(40),o(927)},927:(e,r,o)=>{!function(e){e(".product-slider__wraper").slick({slidesToShow:2,centerMode:!0,centerPadding:"90px",prevArrow:e(".product-slider-nav__prev"),nextArrow:e(".product-slider-nav__next"),asNavFor:".gallery-slider__wraper",swipe:!1,responsive:[{breakpoint:992,settings:{centerPadding:"40px",swipe:!0}},{breakpoint:768,settings:{slidesToShow:1,centerMode:!0,centerPadding:"20px",arrows:!1,draggable:!1,swipe:!0}}]}),e(".gallery-slider__wraper").slick({slidesToShow:1,prevArrow:e(".gallery-slider-nav__prev"),nextArrow:e(".gallery-slider-nav__next"),asNavFor:".product-slider__wraper"});let r=e(".product-info-tabs__item");r.hide().filter(":first").show(),e(".product-info-nav__item a").on("click",(function(){return r.hide(),r.filter(this.hash).show(),e(".product-info-nav__item a").removeClass("active"),e(this).addClass("active"),!1})).filter(":first").on("click"),e("[data-action=open-popup]").on("click",(function(r){r.preventDefault(),function(r){e(".popup-overlay").fadeIn(300,(function(){e("html").css({"overflow-y":"hidden","overflow-x":"hidden"}),e(".popup").addClass("open"),e(r).addClass("open").animate({opacity:1},400)}))}("."+e(this).attr("data-popup")),r.stopPropagation()})),e("[data-action=close-popup]").on("click",(function(r){r.preventDefault(),e(".popup__module").animate({opacity:0},400,(function(){history.pushState("",document.title,window.location.pathname),e(".popup__module").removeClass("open"),e(".popup").removeClass("open"),e("html").css("overflow-y","visible"),e(".popup-overlay").fadeOut(300)}))}))}(o(563))}},o={};function t(e){var a=o[e];if(void 0!==a)return a.exports;var i=o[e]={exports:{}};return r[e].call(i.exports,i,i.exports,t),i.exports}t.m=r,e=[],t.O=(r,o,a,i)=>{if(!o){var n=1/0;for(d=0;d<e.length;d++){o=e[d][0],a=e[d][1],i=e[d][2];for(var p=!0,s=0;s<o.length;s++)(!1&i||n>=i)&&Object.keys(t.O).every((e=>t.O[e](o[s])))?o.splice(s--,1):(p=!1,i<n&&(n=i));if(p){e.splice(d--,1);var l=a();void 0!==l&&(r=l)}}return r}i=i||0;for(var d=e.length;d>0&&e[d-1][2]>i;d--)e[d]=e[d-1];e[d]=[o,a,i]},t.n=e=>{var r=e&&e.__esModule?()=>e.default:()=>e;return t.d(r,{a:r}),r},t.d=(e,r)=>{for(var o in r)t.o(r,o)&&!t.o(e,o)&&Object.defineProperty(e,o,{enumerable:!0,get:r[o]})},t.o=(e,r)=>Object.prototype.hasOwnProperty.call(e,r),(()=>{var e={179:0};t.O.j=r=>0===e[r];var r=(r,o)=>{var a,i,n=o[0],p=o[1],s=o[2],l=0;if(n.some((r=>0!==e[r]))){for(a in p)t.o(p,a)&&(t.m[a]=p[a]);if(s)var d=s(t)}for(r&&r(o);l<n.length;l++)i=n[l],t.o(e,i)&&e[i]&&e[i][0](),e[i]=0;return t.O(d)},o=self.webpackChunkallpricer=self.webpackChunkallpricer||[];o.forEach(r.bind(null,0)),o.push=r.bind(null,o.push.bind(o))})();var a=t.O(void 0,[216],(()=>t(579)));a=t.O(a)})();
