(function ($) {
  // Слайдер с изображениями
  $('.product-slider__wraper').slick({
    slidesToShow: 2,
    centerMode: true,
    centerPadding: '90px',
    prevArrow: $('.product-slider-nav__prev'),
    nextArrow: $('.product-slider-nav__next'),
    asNavFor: '.gallery-slider__wraper',
    swipe: false,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          centerPadding: '40px',
          swipe: true
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          centerPadding: '20px',
          arrows: false,
          draggable: false,
          swipe: true
        }
      }
    ]
  });

  $('.gallery-slider__wraper').slick({
    slidesToShow: 1,
    prevArrow: $('.gallery-slider-nav__prev'),
    nextArrow: $('.gallery-slider-nav__next'),
    asNavFor: '.product-slider__wraper'
  });

  // Вкладки
  let tab = $('.product-info-tabs__item');

  tab.hide().filter(':first').show();

  $('.product-info-nav__item a').on('click', function () {
    tab.hide();
    tab.filter(this.hash).show();
    $('.product-info-nav__item a').removeClass('active');
    $(this).addClass('active');

    return false;
  }).filter(':first').on('click');

  // Функция открытия окна
  function openPopup(url) {
    $('.popup-overlay').fadeIn(300, function () {
      $('html').css({ 'overflow-y': 'hidden', 'overflow-x': 'hidden' });
      $('.popup').addClass('open');
      $(url).addClass('open').animate({ opacity: 1 }, 400);
    });
  }

  // Функция закрытия окна
  function closePopup() {
    $('.popup__module').animate({ opacity: 0 }, 400, function () {
      history.pushState('', document.title, window.location.pathname);

      $('.popup__module').removeClass('open');
      $('.popup').removeClass('open');
      $('html').css('overflow-y', 'visible');
      $('.popup-overlay').fadeOut(300);
    });
  }

  // Открывает окно
  $('[data-action=open-popup]').on('click', function (event) {
    event.preventDefault();

    let url = '.' + $(this).attr('data-popup');

    openPopup(url);
    event.stopPropagation();
  });

  // Закрывает окно
  $('[data-action=close-popup]').on('click', function (event) {
    event.preventDefault();
    closePopup();
  });
}(jQuery));
