# Allpricer

Стили и шрифты с вашего сайта закомментированы в файлах:
/src/index.js и /src/assets/scss/main.css

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
